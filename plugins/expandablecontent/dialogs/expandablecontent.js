/**
 * @file
 * expandablecontent.js
 */

CKEDITOR.dialog.add('expandablecontent', function (editor) {
  'use strict';
  return {
    title: 'Expandable content properties',
    minWidth: 400,
    minHeight: 200,
    // The dialog contents. Here we are collecting the number of expandable
    // sections the user wished to insert.
    contents: [
      {
        id: 'settings',
        label: 'Settings',
        elements: [
          {
            type: 'text',
            id: 'numberOfSections',
            label: 'Number of sections',
            validate: CKEDITOR.dialog.validate.integer(Drupal.t('Number of sections must be an integer.'))
          }
        ]
      }
    ],
    // Called when user clicks OK from within the dialog. Insert our template
    // into the editor.
    onOk: function () {
      var dialog = this;
      // Our template for the expandable content section.
      var expandable_section = '<div class="expandable-section">' +
        '<div class="expandable-title"><h2>' + Drupal.t('Section title') + '</h2></div>' +
        '<div class="expandable-content"><p>' + Drupal.t('Section content') + '</p></div>' +
        '</div>';
      // The user input for how many sections we are inserting.
      var number_of_sections = dialog.getValueOf('settings', 'numberOfSections');
      // The HTML to send back to the editor.
      var full_expandable_section_html = '';
      for (var i = 0; i < number_of_sections; i++) {
        full_expandable_section_html += expandable_section;
      }
      // Add an empty <p> tag so users can insert content below the div. This is
      // a bug in CKEditor in that you can not click out of a div if there is
      // no content below it.
      // See http://dev.ckeditor.com/ticket/5921 for relevant bug report.
      full_expandable_section_html += '<p>&nbsp;</p>';
      // Insert our HTML into the editor.
      editor.insertHtml(full_expandable_section_html);
    }
  };
});
