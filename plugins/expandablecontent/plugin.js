/**
 * @file
 * plugin.js
 */

(function ($) {

  'use strict';

  CKEDITOR.plugins.add('expandablecontent', {
    // CKEditor will look for PNG files in the icons folder.
    icons: 'expandablecontent',
    init: function (editor) {
      editor.addCommand('expandablecontent', new CKEDITOR.dialogCommand('expandablecontent'));
      editor.ui.addButton('expandablecontent', {
        label: Drupal.t('Insert expandable content'),
        command: 'expandablecontent',
        icon: this.path + 'icons/expandablecontent.png',
        toolbar: 'insert'
      });
      CKEDITOR.dialog.add('expandablecontent', this.path + 'dialogs/expandablecontent.js');
    }
  });

})(jQuery);
