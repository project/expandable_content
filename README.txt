CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation


INTRODUCTION
------------

Current Maintainers:
Manish Kumar <manishupdh900@gmail.com>

This module provide a ckeditor plugin to add expandable content, by enabling 
this module you will be able to add expandable content which have multiple 
section with a heading followed by content, by default content is collapsed and 
on click of heading it will expand.

You can use this module along with ckeditor or wysiwyg module, but for wysiwyg 
module editor should be ckeditor.

Note: To use this feature you must have allowed div and classes, otherwise it
will not work.


INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
* After installation just enable the plugin and you are ready to go.
