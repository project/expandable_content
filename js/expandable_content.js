/**
 * @file
 * expandable_content.js
 */

(function ($) {

  'use strict';

  Drupal.behaviors.developerDocs = {
    attach: function () {
      $('.expandable-title').click(function () {
        $(this).parent('.expandable-section').toggleClass('open');
      });
    }
  };

})(jQuery);
